// Given a list of words group the words that are anagrams of each other and output the results
// to stdout with each anagram group on a new line.
//
// Example input: ["bat", "act", "hat", "cat", "tab"]
// Example output:
// bat, tab
// act, cat
// hat
//

package main

import (
	"fmt"
	"strings"
)

type angMap map[string][]string

// Put letter in alpha order to make key
func makeHashKey(ltrs []int) string {
	var key string
	for idx, num := range ltrs {
		if num == 0 {
			continue
		}
		ltrSt := string(rune(idx) + 'a')
		if num == 1 {
			key += ltrSt
		} else {
			key += strings.Repeat(ltrSt, num)
		}
	}
	return key
}

func main() {
	words := []string{"bat", "act", "hat", "cat", "tab", "moove", "voome"}
	anagrams := make(angMap)
	for _, word := range words {
		ltrAry := make([]int, 26)
		// Populate letter array
		for _, let := range word {
			idx := let - 'a'
			ltrAry[idx]++
		}
		// Use array to get the map key, then append new word to array at key
		key := makeHashKey(ltrAry)
		anagrams[key] = append(anagrams[key], word)
	}
	// Print anagrams
	for key, val := range anagrams {
		fmt.Printf("Common letter: '%s', words: %v\n", key, val)
	}
}
